<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wdkecologicadb');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'wdksiteadmin');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '*Acesso@@wdk@@2018*');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B~.Wj<Gh(x#H@Bd$ExB*I}</3F& $XEityR@X}#zSfzhj%RU{T#eAsB:1G8vx-t0');
define('SECURE_AUTH_KEY',  'UbCG5/]+~_ci~g9(xfu;J9kXCmLcP^1pK_H$KcOnR&s5!R323lXJclfo4%]`LbBr');
define('LOGGED_IN_KEY',    'PJ{7f zlVAu +*QXY%{2O>y4ae&lyQ:/?$0x91{;wB>G#]aVq6*!kACkmp{NT9OK');
define('NONCE_KEY',        '[j,[+/XTYq*6wuY]z6Otx1K]B^F06zkC(wm0,-DVm83ws&Ff](^F|Hha5Mu<7csN');
define('AUTH_SALT',        'i!<cF!>Vetp4H3>6=F{6%.Ryj}ka,N+FJ0DJ7r)T&w]W0>HPW!@Pzz.7H#n,Q6u$');
define('SECURE_AUTH_SALT', ';kh^s) ;V=uyj{-5ecK/S)>,F),zDdgi-J(TDua`@XE^QbL`05R#2^F2^CXILz0Q');
define('LOGGED_IN_SALT',   'IueEi;wuc(.Y;+djEG2BA$u,8-.NT2 [YB.A5#o+ss@^@pkH&!R@2x;,0|4ezw;2');
define('NONCE_SALT',       'b+jC@q7D{g:@%]O:GvO; sXd+%UX>!gG_`&0Ve|;Not~7se`lYOCUppIJ?fdN8-_');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');
/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

